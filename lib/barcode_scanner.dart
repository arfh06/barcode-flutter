import 'dart:async';

import 'package:barcode_flutter/detail_item.dart';
import 'package:barcode_flutter/home_screen.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BarcodeScannerScreen extends StatefulWidget {
  @override
  _ScanState createState() => new _ScanState();
}

class _ScanState extends State<BarcodeScannerScreen> {
  String barcode = "";

  @override
  initState() {
    super.initState();
    scan();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('QR Code Scanner'),
      ),
      body: Container(
        color: Colors.black,
      ),
      // new Center(
      //   child: new Column(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     crossAxisAlignment: CrossAxisAlignment.stretch,
      //     children: <Widget>[
      //       Padding(
      //         padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      //         child: RaisedButton(
      //             color: Colors.blue,
      //             textColor: Colors.white,
      //             splashColor: Colors.blueGrey,
      //             onPressed: scan,
      //             child: const Text('START CAMERA SCAN')),
      //       ),
      //       Padding(
      //         padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      //         child: Text(
      //           barcode,
      //           textAlign: TextAlign.center,
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => DetailItemScreen(
                    barcodeId: barcode,
                    dataSrc: 'scan',
                  )));
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)');

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreenState()));
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }
}
