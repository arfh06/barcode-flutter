import 'package:barcode_flutter/barcode_scanner.dart';
import 'package:barcode_flutter/detail_item.dart';
import 'package:barcode_flutter/sqflite_screen.dart';
import 'package:flutter/material.dart';
// import 'package:qr_scanner_generator/scan.dart';
// import 'package:qr_scanner_generator/generate.dart';
import 'package:flutter/rendering.dart';
import 'package:barcode_flutter/database_helper.dart';

class HomeScreenState extends StatefulWidget {
  HomeScreenState();

  HomeScreen createState() => HomeScreen();
}

class HomeScreen extends State<HomeScreenState> {
  List<Map<String, dynamic>> items = new List();
  DatabaseHelper dbHelper = DatabaseHelper.instance;
  bool isListHidden = true, isEmptyTextHidden = true;

  @override
  void initState() {
    super.initState();
    _query();
  }

  void _query() async {
    final allRows = await dbHelper.queryAllRows();
    print('query all rows: $allRows');
    // allRows.forEach((row) => items.add(row['item_name']));

    if (allRows.length > 0 || allRows.isNotEmpty || allRows != null) {
      setState(() {
        allRows.forEach((row) => items.add(row));

        isListHidden = false;
        isEmptyTextHidden = true;
        print(allRows.length.toString());
        print(items.length.toString() + ', ' + items[0]['barcode_id']);
      });
    } else {
      setState(() {
        isListHidden = true;
        isEmptyTextHidden = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Text('History', style: TextStyle(color: Colors.black)),
      ),
      body: Container(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Offstage(
            offstage: false,
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: RaisedButton(
                  color: Colors.green,
                  textColor: Colors.white,
                  splashColor: Colors.lightGreen[300],
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => BarcodeScannerScreen()),
                    );
                  },
                  child: const Text('SCAN')),
            ),
          ),
          Expanded(
            child: Offstage(
              offstage: isListHidden,
              child: ListView.builder(
                  padding: const EdgeInsets.all(8.0),
                  itemCount: items.length,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(
                        '${items[index]['barcode_id']}',
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                      subtitle: Text(
                        '${items[index]['item_name']}',
                        style: TextStyle(fontSize: 10),
                      ),
                      trailing: Icon(Icons.arrow_forward_ios),
                      // trailing: Image.asset(
                      //   'assets/ic_arrow_fwd.png',
                      //   scale: 1.0,
                      //   height: 10,
                      //   width: 10,
                      // ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailItemScreen(
                                    barcodeId: items[index]['barcode_id'],
                                    dataSrc: 'detail',
                                  )),
                        );
                      },
                    );
                  }),
            ),
          ),
          Offstage(
            offstage: isEmptyTextHidden,
            child: Align(
              alignment: Alignment.center,
              child: Text('No Barcode Scanned.'),
            ),
          )
        ],
      )),
    );
  }
}
