import 'package:barcode_flutter/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:barcode_flutter/database_helper.dart';
import 'package:intl/intl.dart';

class SqfliteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQFlite Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;

  //variables
  String name = 'query';

  // homepage layout
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('sqflite'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text(
                'insert',
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                _insert(context);
              },
            ),
            RaisedButton(
              child: Text(
                name,
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                _query();
              },
            ),
            RaisedButton(
              child: Text(
                'update',
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                _update();
              },
            ),
            RaisedButton(
              child: Text(
                'delete',
                style: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                _delete();
              },
            ),
          ],
        ),
      ),
    );
  }

  // Button onPressed methods

  void _insert(BuildContext context) async {
    // row to insert
    Map<String, dynamic> row = {
      DatabaseHelper.columnBarcodeId: 'PRD190331-00000001',
      DatabaseHelper.columnItemName: 'Sendok',
      DatabaseHelper.columnDate:
          new DateFormat('"dd MMM, hh:mm aaa"').format(new DateTime.now())
    };
    final id = await dbHelper.insert(row);
    print('inserted row id: $id');

    // Navigator.of(context).pop(true);
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomeScreenState()));
  }

  void _query() async {
    final allRows = await dbHelper.queryAllRows();
    print('query all rows:');
    allRows.forEach((row) => print(row));

    List list = new List();
    list = allRows.toList();
    name = list[0]['item_name'];
    print('list 0 $name');
  }

  void _update() async {
    // row to update
    Map<String, dynamic> row = {
      DatabaseHelper.columnId: 1,
      DatabaseHelper.columnBarcodeId: 'PRD124',
      DatabaseHelper.columnItemName: 'Garpu',
      DatabaseHelper.columnDate:
          new DateFormat('"dd MMM, hh:mm aaa"').format(new DateTime.now())
    };
    final rowsAffected = await dbHelper.update(row);
    print('updated $rowsAffected row(s)');
  }

  void _delete() async {
    // Assuming that the number of rows is the id for the last row.
    final id = await dbHelper.queryRowCount();
    final rowsDeleted = await dbHelper.delete(id);
    print('deleted $rowsDeleted row(s): row $id');
  }
}
