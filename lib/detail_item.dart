import 'dart:convert';

import 'package:barcode_flutter/barcode_scanner.dart';
import 'package:barcode_flutter/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:barcode_flutter/database_helper.dart';
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';

part 'example.g.dart';

class DetailItemScreen extends StatefulWidget {
  final String barcodeId;
  String dataSrc;

  // DetailItemScreen();
  DetailItemScreen({Key key, @required this.barcodeId, this.dataSrc})
      : super(key: key);

  _detailItemState createState() => _detailItemState();
}

// baseUrl: "https://5d42a6e2bc64f90014a56ca0.mockapi.io/api/v1/"
@RestApi(baseUrl: "https://17092.scanner.shweapi.com/api")
abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;

  @GET("/tasks")
  Future<List<Task>> getTasks();

  @GET("/tasks/{id}")
  Future<Task> getTask(@Path("id") String id);

  @PATCH("/tasks/{id}")
  Future<Task> updateTaskPart(
      @Path() String id, @Body() Map<String, dynamic> map);

  @PUT("/tasks/{id}")
  Future<Task> updateTask(@Path() String id, @Body() Task task);

  @DELETE("/tasks/{id}")
  Future<void> deleteTask(@Path() String id);

  @POST("/tasks")
  Future<Task> createTask(@Body() Task task);

  @POST("/product")
  Future<Task> getProduct(@Body() String idProduct);
}

final baseUrl = 'https://17092.scanner.shweapi.com/api';
List<Data> data;
String idProduct = '';
String nameProduct = '';
String imageProduct = '';
String typeProduct = '';
String uomProduct = '';
String descProduct = '';
String purchaseAccProduct = '';
String purchaseReturnAccProduct = '';
String initQtyProduct = '';
String currentPurchasePriceProducts = '';
String isLocationProduct = '';
String isWarehouseProduct = '';

@JsonSerializable()
class Task {
  String message;
  List<dynamic> success;

  Task({this.message, this.success});

  factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
  Map<String, dynamic> toJson() => _$TaskToJson(this);
}

@JsonSerializable()
class Data {
  String idProduct;
  String nameProduct;
  String imageProduct;
  String typeProduct;
  String uomProduct;
  String descProduct;
  String purchaseAccProduct;
  String purchaseReturnAccProduct;
  int initQtyProduct;
  int currentPurchasePriceProduct;
  bool isLocationProduct;
  bool isWarehouseProduct;

  Data({
    this.idProduct,
    this.nameProduct,
    this.imageProduct,
    this.typeProduct,
    this.uomProduct,
    this.descProduct,
    this.purchaseAccProduct,
    this.purchaseReturnAccProduct,
    this.initQtyProduct,
    this.currentPurchasePriceProduct,
    this.isLocationProduct,
    this.isWarehouseProduct,
  });

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
  Map<String, dynamic> toJson() => _$DataToJson(this);
}

class _detailItemState extends State<DetailItemScreen> {
  // reference to our single class that manages the database
  final dbHelper = DatabaseHelper.instance;
  ProgressDialog prgDialog;

  @override
  initState() {
    super.initState();

    final dio = Dio(); // Provide a dio instance
    dio.options.headers["Demo-Header"] =
        "demo header"; // config your dio headers globally
    dio.options.headers["Content-Type"] = "application/json";
    final client = RestClient(dio);

    print('data passed ' + widget.barcodeId);

    //define progress dialog
    SchedulerBinding.instance
        .addPostFrameCallback((_) => showProgressDialog(context));

    //getHttp('PRD190331-00000001');
    getHttp(widget.barcodeId);
  }

  void getHttp(param) async {
    try {
      Response response =
          await Dio().post("$baseUrl/product", data: {"id_product": param});

      Task task = Task.fromJson(response.data);
      print(task.message);

      data = task.success.map((i) => Data.fromJson(i)).toList();
      print(data[0].idProduct);

      setState(() {
        idProduct = data[0].idProduct;
        nameProduct = data[0].nameProduct;
        imageProduct = data[0].imageProduct;
        typeProduct = data[0].typeProduct;
        uomProduct = data[0].uomProduct;
        descProduct = data[0].descProduct;
        initQtyProduct = data[0].initQtyProduct.toString();
        currentPurchasePriceProducts =
            data[0].currentPurchasePriceProduct.toString();
        purchaseAccProduct = data[0].purchaseAccProduct;
        purchaseReturnAccProduct = data[0].purchaseReturnAccProduct;

        if (data[0].isLocationProduct.toString().isNotEmpty) {
          isLocationProduct = data[0]
                  .isLocationProduct
                  .toString()
                  .substring(0, 1)
                  .toUpperCase() +
              data[0].isLocationProduct.toString().substring(1);
        }
        if (data[0].isWarehouseProduct.toString().isNotEmpty) {
          isWarehouseProduct = data[0]
                  .isWarehouseProduct
                  .toString()
                  .substring(0, 1)
                  .toUpperCase() +
              data[0].isWarehouseProduct.toString().substring(1);
        }

        if (widget.dataSrc.trim().toLowerCase() == 'scan') {
          _insert(context);
        }
      });

      if (prgDialog.isShowing()) {
        prgDialog.dismiss();
      }
    } catch (e) {
      print(e);
    }
  }

  void _insert(BuildContext context) async {
    // row to insert
    Map<String, dynamic> row = {
      DatabaseHelper.columnBarcodeId: idProduct,
      DatabaseHelper.columnItemName: nameProduct,
      DatabaseHelper.columnDate:
          new DateFormat('"dd MMM, hh:mm aaa"').format(new DateTime.now())
    };
    final id = await dbHelper.insert(row);
    print('inserted row id: $id');
  }

  void showProgressDialog(BuildContext context) {
    prgDialog = new ProgressDialog(context,
        isDismissible: false, type: ProgressDialogType.Normal);
    prgDialog.style(
      progressWidget: Container(
        padding: EdgeInsets.all(10.0),
        child: CircularProgressIndicator(),
      ),
      message: 'Processing...',
      elevation: 8.0,
      borderRadius: 8.0,
    );

    prgDialog.show();
  }

  @override
  Widget build(BuildContext context) {
    AppBar appBar = AppBar(
        backgroundColor: Colors.grey[200],
        centerTitle: true,
        elevation: 0.0,
        title: Text(
          'Product Details',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        leading: GestureDetector(
          child: Image.asset(
            'assets/ic_button_back.png',
            scale: 1.0,
          ),
          onTap: () => Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => HomeScreenState())),
        ));

    return Scaffold(
      appBar: appBar,
      body: Container(
        height:
            (MediaQuery.of(context).size.height - appBar.preferredSize.height),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: (MediaQuery.of(context).size.height -
                            appBar.preferredSize.height) /
                        2,
                    child: FadeInImage.assetNetwork(
                        placeholder: 'assets/ic_arrow_fwd.png',
                        image: imageProduct,
                        fit: BoxFit.fill),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: (MediaQuery.of(context).size.height -
                          appBar.preferredSize.height) /
                      2,
                  color: Colors.grey[300],
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    margin: EdgeInsets.all(16.0),
                    height: (MediaQuery.of(context).size.height -
                                appBar.preferredSize.height) /
                            2 +
                        60,
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 60.0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(
                                          16.0, 8.0, 8.0, 8.0),
                                      width:
                                          MediaQuery.of(context).size.width / 2,
                                      child: Text(
                                        '\$$currentPurchasePriceProducts',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin:
                                          EdgeInsets.fromLTRB(8.0, 0, 16.0, 0),
                                      child: Text(
                                        'Total : $initQtyProduct pcs',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: 1.0,
                          color: Colors.grey[200],
                        ),
                        Expanded(
                          flex: 8,
                          child: SingleChildScrollView(
                            child: ConstrainedBox(
                              constraints: BoxConstraints(),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 8.0, 8.0),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Product ID',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                idProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              8.0, 0, 16.0, 0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Product Type',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                typeProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 8.0, 8.0),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Product Name',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                nameProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              8.0, 0, 16.0, 0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Description',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                descProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 8.0, 8.0),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Unit Of Measure',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                uomProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              8.0, 0, 16.0, 0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Initial Quality',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                initQtyProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 8.0, 8.0),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Purchase Account',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                purchaseAccProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              8.0, 0, 16.0, 0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Purchase Return Account',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                purchaseReturnAccProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              16.0, 8.0, 8.0, 8.0),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              2,
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Is Location',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                isLocationProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Container(
                                          margin: EdgeInsets.fromLTRB(
                                              8.0, 0, 16.0, 0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.max,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                'Is Warehouse',
                                                style: TextStyle(
                                                  color: Colors.black38,
                                                  fontSize: 12.0,
                                                ),
                                              ),
                                              Text(
                                                isWarehouseProduct,
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18.0,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            child: Container(
                              margin:
                                  EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                              height: appBar.preferredSize.height,
                              width: MediaQuery.of(context).size.width,
                              child: RaisedButton(
                                color: Colors.green[800],
                                child: Text(
                                  'SCAN',
                                  style: TextStyle(color: Colors.white),
                                ),
                                onPressed: () {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            BarcodeScannerScreen()),
                                  );
                                },
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
