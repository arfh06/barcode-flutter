// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Task _$TaskFromJson(Map<String, dynamic> json) {
  return Task(
    message: json['message'] as String,
    success: json['success'] as List<dynamic>,
  );
}

Map<String, dynamic> _$TaskToJson(Task instance) => <String, dynamic>{
      'message': instance.message,
      'success': instance.success,
    };

Data _$DataFromJson(Map<String, dynamic> json) {
  return Data(
    idProduct: json['id_products'] as String,
    nameProduct: json['name_products'] as String,
    imageProduct: json['image_products'] as String,
    typeProduct: json['type_products'] as String,
    descProduct: json['description_products'] as String,
    uomProduct: json['uom_products'] as String,
    purchaseAccProduct: json['purchase_account_products'] as String,
    purchaseReturnAccProduct:
        json['purchase_return_account_products'] as String,
    currentPurchasePriceProduct: json['current_purchase_price_products'] as int,
    initQtyProduct: json['initial_quantity_products'] as int,
    isLocationProduct: json['is_location_products'] as bool,
    isWarehouseProduct: json['is_warehouse_products'] as bool,
  );
}

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'id_products': instance.idProduct,
      'name_products': instance.nameProduct,
      'image_products': instance.imageProduct,
      'type_products': instance.typeProduct,
      'description_products': instance.descProduct,
      'uom_products': instance.uomProduct,
      'purchase_account_products': instance.purchaseAccProduct,
      'purchase_return_account_products': instance.purchaseReturnAccProduct,
      'init_quantity_products': instance.initQtyProduct,
      'current_purchase_price_products': instance.currentPurchasePriceProduct,
      'is_location_products': instance.isLocationProduct,
      'is_warehouse_products': instance.isWarehouseProduct,
    };

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio) {
    ArgumentError.checkNotNull(_dio, '_dio');
  }

  final Dio _dio;

  // final String baseUrl = 'https://5d42a6e2bc64f90014a56ca0.mockapi.io/api/v1/';
  final String baseUrl = 'https://17092.scanner.shweapi.com/api';

  @override
  getTasks() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<List<dynamic>> _result = await _dio.request('/tasks',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    var value = _result.data
        .map((dynamic i) => Task.fromJson(i as Map<String, dynamic>))
        .toList();
    return Future.value(value);
  }

  @override
  getProduct(task) async {
    ArgumentError.checkNotNull(task, 'task');

    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    // Response _result = await Dio().post('/product');
    final Response _result = await _dio.post('/product',
        queryParameters: queryParameters,
        options:
            RequestOptions(method: 'POST', extra: _extra, baseUrl: baseUrl),
        data: _data);
    print('response ' + _result.extra.values.toString());
    final value = Task.fromJson(_result.data);
    print('request ' + _result.data.values.toString());
    return Future.value(value);
  }

  @override
  getTask(id) async {
    ArgumentError.checkNotNull(id, 'id');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/tasks/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = Task.fromJson(_result.data);
    return Future.value(value);
  }

  @override
  updateTaskPart(id, map) async {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(map, 'map');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(map ?? <String, dynamic>{});
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/tasks/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PATCH',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = Task.fromJson(_result.data);
    return Future.value(value);
  }

  @override
  updateTask(id, task) async {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(task, 'task');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(task ?? <String, dynamic>{});
    final Response<Map<String, dynamic>> _result = await _dio.request(
        '/tasks/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'PUT',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = Task.fromJson(_result.data);
    return Future.value(value);
  }

  @override
  deleteTask(id) async {
    ArgumentError.checkNotNull(id, 'id');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final Response<void> _result = await _dio.request('/tasks/$id',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'DELETE',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    return Future.value(null);
  }

  @override
  createTask(task) async {
    ArgumentError.checkNotNull(task, 'task');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(task ?? <String, dynamic>{});
    final Response<Map<String, dynamic>> _result = await _dio.request('/tasks',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = Task.fromJson(_result.data);
    return Future.value(value);
  }
}
